package main

import (
	"bytes"
	"fmt"
	"github.com/spf13/viper"
	"text/template"
	"net/http"
	"sort"
)

func GenMaddash(r *http.Request) (string, error) {
	PerfsonarURL := fmt.Sprintf("https://perfsonar.%s/esmond/perfsonar/archive", viper.GetString("cluster_url"))
	if t, err := template.ParseFiles("templates/maddash.yaml"); err != nil {
		return "", err
	} else {
		conf := MaddashConfig{Nodes: []Node{}, Groups: map[string][]Node{}, PerfsonarURL: PerfsonarURL}
		for nodeID := range viper.Get("nodes").(map[string]interface{}) {
			node := Node{
				IP:       viper.GetString(fmt.Sprintf("nodes.%s.ip", nodeID)),
				Hostname: viper.GetString(fmt.Sprintf("nodes.%s.hostname", nodeID)),
				IPV6: viper.GetBool(fmt.Sprintf("nodes.%s.ipv6", nodeID)),
			}
			conf.Nodes = append(conf.Nodes, node)

			for _, group := range viper.GetStringSlice(fmt.Sprintf("nodes.%s.groups", nodeID)) {
				if conf.Groups[group] == nil {
					conf.Groups[group] = []Node{}
				}
				conf.Groups[group] = append(conf.Groups[group], node)
			}
		}

		sort.Sort(byDomain(conf.Nodes))

		var tpl bytes.Buffer
		if err = t.Execute(&tpl, conf); err != nil {
			return "", err
		} else {
			return tpl.String(), nil
		}
	}
}

func GenMaddashConfig(r *http.Request) (string, error) {
	if t, err := template.ParseFiles("templates/config.json"); err != nil {
		return "", err
	} else {
		var tpl bytes.Buffer
		if err = t.Execute(&tpl, nil); err != nil {
			return "", err
		} else {
			return tpl.String(), nil
		}
	}
}

func GenTransferMesh(r *http.Request) (string, error) {
	if t, err := template.ParseFiles("templates/cron-gridftp-transfer-mesh.sh"); err != nil {
		return "", err
	} else {
		conf := MaddashConfig{Nodes: []Node{}}
		for nodeID := range viper.Get("nodes").(map[string]interface{}) {
			node := Node{
				IP:       viper.GetString(fmt.Sprintf("nodes.%s.ip", nodeID)),
				Hostname: viper.GetString(fmt.Sprintf("nodes.%s.hostname", nodeID)),
				IPV6: viper.GetBool(fmt.Sprintf("nodes.%s.ipv6", nodeID)),
			}
			conf.Nodes = append(conf.Nodes, node)
		}

		var tpl bytes.Buffer
		if err = t.Execute(&tpl, conf); err != nil {
			return "", err
		} else {
			return tpl.String(), nil
		}
	}
}

func GenTiming(r *http.Request) (string, error) {
	if t, err := template.ParseFiles("templates/cron-mesh-timing.sh"); err != nil {
		return "", err
	} else {
		for nodeID := range viper.Get("nodes").(map[string]interface{}) {
			if viper.GetString(fmt.Sprintf("nodes.%s.hostname", nodeID)) == r.URL.Query().Get("hostname") {
				var tpl bytes.Buffer
				if err = t.Execute(&tpl, CronConfig{viper.GetString(fmt.Sprintf("nodes.%s.cron", nodeID)), viper.GetString(fmt.Sprintf("nodes.%s.cron_res", nodeID))}); err != nil {
					return "", err
				}
				return tpl.String(), nil
			}
		}
		return "", fmt.Errorf("Not found or no 'hostname' argument")
	}
}

func GenLoadGridftp(r *http.Request) (string, error) {
	if t, err := template.ParseFiles("templates/cron-load-gridftp.sh"); err != nil {
		return "", err
	} else {
		var tpl bytes.Buffer
		if err = t.Execute(&tpl, nil); err != nil {
			return "", err
		} else {
			return tpl.String(), nil
		}
	}
}

func GenGpn(r *http.Request) (string, error) {
	if t, err := template.ParseFiles("templates/gpn.json"); err != nil {
		return "", err
	} else {
		var tpl bytes.Buffer
		if err = t.Execute(&tpl, nil); err != nil {
			return "", err
		} else {
			return tpl.String(), nil
		}
	}
}
