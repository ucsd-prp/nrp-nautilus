package main

import (
	"github.com/spf13/viper"
	"fmt"
	"log"
	"net/http"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	"time"
	"k8s.io/api/core/v1"
	"strings"
)
	
type Node struct {
	IP string
	Hostname string
	IPV6 bool
}

type MaddashConfig struct {
	Nodes []Node
	PerfsonarURL string
	Groups map[string][]Node
}

type CronConfig struct {
	Cron string
	CronRes string
}

type byDomain []Node

func (s byDomain) Len() int {
	return len(s)
}
func (s byDomain) Swap(i, j int) {
	s[i], s[j] = s[j], s[i]
}
func (s byDomain) Less(i, j int) bool {
	iSep := strings.Split(s[i].Hostname,".")
	if len(iSep) > 3 {
		iSep = iSep[len(iSep)-3:]
	} else {
		iSep = iSep[len(iSep)-2:]
	}

	jSep := strings.Split(s[j].Hostname,".")
	if len(jSep) > 3 {
		jSep = jSep[len(jSep)-3:]
	} else {
		jSep = jSep[len(jSep)-2:]
	}


	return strings.Compare(
		strings.Join(iSep, "."),
		strings.Join(jSep, "."),
		) < 0
}

type FileGenFunc func(r *http.Request) (string, error)

func GenFile(fun FileGenFunc, w http.ResponseWriter, r *http.Request) {
	if ret, err := fun(r); err != nil {
		w.Write([]byte(err.Error()))
	} else {
		w.Write([]byte(ret))
	}
}

func main() {
	viper.SetConfigName("config")
	viper.AddConfigPath(".")
	viper.AddConfigPath("config")
	err := viper.ReadInConfig()
	if err != nil {
		panic(fmt.Errorf("fatal error config file: %s", err))
	}

	k8sconfig, err := rest.InClusterConfig()
	if err != nil {
		log.Fatal("Failed to do inclusterconfig: " + err.Error())
		return
	}

	clientset, err := kubernetes.NewForConfig(k8sconfig)
	if err != nil {
		log.Fatal("Failed to do inclusterconfig new client: " + err.Error())
	}

	go func(){
		ticker := time.NewTicker(time.Minute)
		defer ticker.Stop()
		for ; true; <-ticker.C {
			//newConfWWW := v1.ConfigMap{
			//	ObjectMeta: metav1.ObjectMeta{
			//		Name: "perfsonar-www",
			//	},
			//	Data: map[string]string{
			//	},
			//}
			//
			//if text, err := GenTransferMesh(); err == nil {
			//	newConfWWW.Data["cron-gridftp-transfer-mesh.sh"] = text
			//}
			//if text, err := GenTiming(); err == nil {
			//	newConfWWW.Data["cron-mesh-timing.sh"] = text
			//}
			//if text, err := GenLoadGridftp(); err == nil {
			//	newConfWWW.Data["cron-load-gridftp.sh"] = text
			//}
			//if text, err := GenTransferMesh(); err == nil {
			//	newConfWWW.Data["cron-gridftp-transfer-mesh.sh"] = text
			//}

			//clientset.CoreV1().ConfigMaps("nrp-perfsonar").Update(&newConfWWW)

			newConfMaddashServer := v1.ConfigMap{
				ObjectMeta: metav1.ObjectMeta{
					Name: "perfsonar-maddash-server",
				},
				Data: map[string]string{
					"log4j.properties": Log4JConfig,
				},
			}

			if text, err := GenMaddash(nil); err == nil {
				newConfMaddashServer.Data["maddash.yaml"] = text
			}

			fmt.Printf("Updating server config\n")
			if _, err := clientset.CoreV1().ConfigMaps("nrp-perfsonar").Get("perfsonar-maddash-server", metav1.GetOptions{}); err == nil {
				if _, err := clientset.CoreV1().ConfigMaps("nrp-perfsonar").Update(&newConfMaddashServer); err != nil {
					fmt.Printf("Error updating config: %s\n", err.Error())
				}
			} else {
				if _, err := clientset.CoreV1().ConfigMaps("nrp-perfsonar").Create(&newConfMaddashServer); err != nil {
					fmt.Printf("Error creating config: %s\n", err.Error())
				}
			}


			newConfMaddashWebui := v1.ConfigMap{
				ObjectMeta: metav1.ObjectMeta{
					Name: "perfsonar-maddash-webui",
				},
				Data: map[string]string{
				},
			}

			if text, err := GenMaddashConfig(nil); err == nil {
				newConfMaddashWebui.Data["config.json"] = text
			}

			fmt.Printf("Updating webui config\n")
			if _, err := clientset.CoreV1().ConfigMaps("nrp-perfsonar").Get("perfsonar-maddash-webui", metav1.GetOptions{}); err == nil {
				if _, err := clientset.CoreV1().ConfigMaps("nrp-perfsonar").Update(&newConfMaddashWebui); err != nil {
					fmt.Printf("Error updating config: %s\n", err.Error())
				}
			} else {
				if _, err := clientset.CoreV1().ConfigMaps("nrp-perfsonar").Create(&newConfMaddashWebui); err != nil {
					fmt.Printf("Error creating config: %s\n", err.Error())
				}
			}
		}
	}()

	http.HandleFunc("/maddash.yaml", func(w http.ResponseWriter, r *http.Request) {
		GenFile(GenMaddash, w, r)
	})
	http.HandleFunc("/cron-gridftp-transfer-mesh.sh", func(w http.ResponseWriter, r *http.Request) {
		GenFile(GenTransferMesh, w, r)
	})
	http.HandleFunc("/cron-load-gridftp.sh", func(w http.ResponseWriter, r *http.Request) {
		GenFile(GenLoadGridftp, w, r)
	})
	http.HandleFunc("/cron-mesh-timing.sh", func(w http.ResponseWriter, r *http.Request) {
		GenFile(GenTiming, w, r)
	})
	http.HandleFunc("/gpn.json", func(w http.ResponseWriter, r *http.Request) {
		GenFile(GenGpn, w, r)
	})
	log.Fatal(http.ListenAndServe(":80", nil))
}

var Log4JConfig string = `log4j.rootCategory=WARN

#normal MadDash log
log4j.logger.net.es.maddash=INFO, MADDASH
log4j.appender.MADDASH=org.apache.log4j.RollingFileAppender
log4j.appender.MADDASH.MaxFileSize=1MB
log4j.appender.MADDASH.MaxBackupIndex=3
log4j.appender.MADDASH.File=/var/log/maddash/maddash-server.log
log4j.appender.MADDASH.layout=org.apache.log4j.PatternLayout
log4j.appender.MADDASH.layout.ConversionPattern=%p %d{ISO8601} %m%n

#NetLogger
log4j.logger.netlogger=INFO, NETLOGGER
log4j.appender.NETLOGGER=org.apache.log4j.RollingFileAppender
log4j.appender.NETLOGGER.MaxFileSize=1MB
log4j.appender.NETLOGGER.MaxBackupIndex=3
log4j.appender.NETLOGGER.File=/var/log/maddash/maddash-server.netlogger.log
log4j.appender.NETLOGGER.layout=org.apache.log4j.PatternLayout
log4j.appender.NETLOGGER.layout.ConversionPattern=level=%p %m%n`