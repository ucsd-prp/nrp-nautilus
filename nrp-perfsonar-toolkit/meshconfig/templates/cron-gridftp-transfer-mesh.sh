#!/bin/bash
export GLOBUS_TCP_PORT_RANGE=50000,51000
export GLOBUS_TCP_SOURCE_RANGE=50000,51000
{{ range .Nodes }}
/usr/local/bin/timeout.sh -t 2000 globus-url-copy -vb -fast -p 16 {{if .IPV6}}-ipv6{{end}} ftp://{{.Hostname}}:2811/export/data/10G.dat file:///export/data/10G.out
{{end}}
source /usr/local/bin/custom.sh # optional local custom config