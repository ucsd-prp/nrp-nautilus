#!/bin/bash
TMPFILE=`mktemp`
python /usr/bin/esmond-ps-load-gridftp -p /opt/esmond-gridftp/perfsonar.nrp-nautilus.io.pickle -U https://perfsonar.nrp-nautilus.io/esmond -u user -k d30ca009ced464c9cea1b81a6e085b00e232a498 -f /var/log/gridftp-transfer.log

curl -s https://perfsonar.nrp-nautilus.io/cron-gridftp-transfer-mesh.sh -o /usr/local/bin/cron-gridftp-transfer-mesh.sh
chmod 755 /usr/local/bin/cron-gridftp-transfer-mesh.sh
sed -i '/'"$HOSTNAME"'/s/^/#/' /usr/local/bin/cron-gridftp-transfer-mesh.sh

curl -s https://perfsonar.nrp-nautilus.io/cron-mesh-timing.sh -o $TMPFILE

MY_HOSTNAME=`hostname -f`

grep -iq $MY_HOSTNAME $TMPFILE
RC=$?

if [[ $RC -eq 0 ]];then
      grep -i $MY_HOSTNAME $TMPFILE > /etc/cron.d/cron-gridftp-transfer-mesh
else
      logger -t "$0" 'Error while downloading cronjob files'
fi

rm -f $TMPFILE