<h1>Join the NRP gridftp maddash :</h1>
<br>
Let us know the IP and FQHN of the system <br>
https://rocket.nautilus.optiputer.net/channel/nautilus-ops <br>
<br>
<h1>1 - Pull config:</h1>
<br>
pip install --upgrade esmond-client<br>
curl -s https://gitlab.com/ucsd-prp/nrp-nautilus/raw/master/nrp-perfsonar-toolkit/meshconfig/cron-load-gridftp.sh -o /usr/local/bin/cron-load-gridftp.sh<br>
chmod 755 /usr/local/bin/cron-load-gridftp.sh<br>
curl -s https://gitlab.com/ucsd-prp/nrp-nautilus/raw/master/nrp-perfsonar-toolkit/meshconfig/timeout.sh -o /usr/local/bin/timeout.sh<br>
chmod 755 /usr/local/bin/timeout.sh<br>
<br>
<h1>2 - Pull new gridftp mesh config :</h1><br>
/usr/local/bin/cron-load-gridftp.sh<br>
<br>
<h1>3 - Pull 10G.dat file from the NRP gridftp mesh :</h1><br>
/usr/local/bin/cron-gridftp-transfer-mesh.sh<br>
<br>
Once an hour all hosts will upload their test results visible here.<br>
https://perfsonar.nrp-nautilus.io/maddash-webui/index.cgi<br>
<br>

<h1>CLUSTER ADMIN ONLY:</h1>

<pre>
[root@controller1 meshconfig]# make pushconfig
kubectl delete configmap meshconfig-conf -n nrp-perfsonar
configmap "meshconfig-conf" deleted
kubectl create configmap -n nrp-perfsonar meshconfig-conf --from-file=config.toml
configmap/meshconfig-conf created

[root@controller1 meshconfig]# kubectl delete pod -l k8s-app=meshconfig -n nrp-perfsonar
pod "meshconfig-5c9c78446f-xvp8h" deleted

[root@controller1 meshconfig]# kubectl get pods -n nrp-perfsonar
NAME                                READY     STATUS    RESTARTS   AGE
esmond-cassandra-0                  1/1       Running   0          5d
esmond-cassandra-1                  1/1       Running   0          5d
esmond-cassandra-2                  1/1       Running   0          5d
esmond-cassandra-3                  1/1       Running   0          5d
meshconfig-5c9c78446f-z9d2k         1/1       Running   0          15s
perfsonar-toolkit-794b46f57-x6nbv   1/1       Running   0          21h

[root@controller1 meshconfig]# kubectl exec -it perfsonar-toolkit-794b46f57-x6nbv bash -n nrp-perfsonar
[root@perfsonar-toolkit-794b46f57-x6nbv /]# pkill -f java
[root@perfsonar-toolkit-794b46f57-x6nbv /]# supervisorctl start maddash

</pre>
