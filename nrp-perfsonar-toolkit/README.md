<h1>Lanuch NRP MaDDash:</h1><br>
kubectl create -f perfsonar-toolkit.yaml -n nrp-perfsonar<br>
kubectl delete -f perfsonar-toolkit.yaml -n nrp-perfsonar<br>

<h1>Cassandra replication:</h1><br>
echo "ALTER KEYSPACE esmond WITH REPLICATION = { 'class' : 'SimpleStrategy', 'replication_factor' : 2 };" | cqlsh esmond-cassandra
